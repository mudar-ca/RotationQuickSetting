# Rotation Quick Setting

Android 7.0+ Quick Settings tile to select portrait or landscape screen orientation.

[![Rotation Quick Setting][logo_rotationqs]][link_rotationqs_playstore]


## FEATURES
* Toggle device orientation: portrait, landscape or auto‑rotation.
* Quick and easy access using Android Quick Settings tiles, without using notifications.
* Does not force unsupported orientation for third‑party apps.
* Supports Android Nougat 7.0 and up.
* Software fix for Nexus 7 auto‑rotation issue.

App compatibility can vary depending on manufacturers. Please verify that your device does support Android TileService API.

## LINKS

* [RotationQS website][link_rotationqs_website]
* [Help: how to edit Quick Settings][link_quick_settings_help]
* [Privacy policy][link_rotationqs_privacy]
* [Download latest APK, ZIP or GZ][link_latest_release]
* [Rotation Quick Setting on Google Play][link_rotationqs_playstore]

[![Android app on Google Play][img_playstore_badge]][link_rotationqs_playstore]


## CREDITS

Android app developed by [Mudar Noufal][link_mudar_ca] &lt;<mn@mudar.ca>&gt;.

The Android app includes libraries and derivative work of the following projects:

* [AOSP][link_lib_aosp] &copy; The Android Open Source Project.
* [Android Support Library v7][link_lib_support] &copy; The Android Open Source Project.
* [Android Asset Studio][link_android_asset_studio] &copy; Roman Nurik. Used to create icons assets.

These three projects are all released under the [Apache License v2.0][link_apache].

App also relies on [Crashlytics SDK][link_crashlytics] for Android © Google Inc. The reason why internet permission is needed.

## CODE LICENSE

    Rotation Quick Setting
    Add a Quick Settings tile to select portrait or landscape screen orientation.

    Copyright (C) 2017 Mudar Noufal <mn@mudar.ca>

    This file is part of RotationQS.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

[logo_rotationqs]: http://rotationqs.mudar.ca/img/logo_rotationqs.png
[img_playstore_badge]: http://rotationqs.mudar.ca/img/en_badge_web_generic_200px.png
[link_rotationqs_playstore]: https://play.google.com/store/apps/details?id=ca.mudar.rotationquicksetting
[link_rotationqs_website]: http://rotationqs.mudar.ca/
[link_rotationqs_privacy]: http://rotationqs.mudar.ca/privacy.html
[link_mudar_ca]: http://www.mudar.ca/
[link_quick_settings_help]: https://support.google.com/android/answer/6111329
[link_latest_release]: https://github.com/mudar/RotationQuickSetting/releases/latest
[link_lib_aosp]: http://source.android.com/
[link_lib_support]: http://developer.android.com/tools/support-library/
[link_android_asset_studio]: http://romannurik.github.io/AndroidAssetStudio/
[link_apache]: http://www.apache.org/licenses/LICENSE-2.0
[link_crashlytics]: https://fabric.io/kits/android/crashlytics/
