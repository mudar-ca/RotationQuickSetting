## Title
*30 chars*

Rotation Quick Setting

## Short description
*80 chars* 

Paramètre de configuration rapide pour choisir une position portait ou paysage de l’écran.

## Full description
*4000 chars*

Intended for Nexus 7 devices with accelerometer auto-rotation issues.

Projet de logiciel libre, disponible sur GitLab. Le code source est publié sous la licence GPLv3 de la Free Software Foundation: https://gitlab.com/mudar-ca/RotationQuickSetting
