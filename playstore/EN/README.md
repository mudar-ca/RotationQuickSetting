## Title
*30 chars*

Rotation Quick Setting

## Short description
*80 chars* 

Quick settings tile to select portrait or landscape screen orientation.

## Full description
*4000 chars*

Quick settings tile to select portrait or landscape screen orientation.
Intended for Nexus 7 devices with accelerometer auto-rotation issues.

Open source project, available on GitLab. Source code is released under the GPLv3 license from the Free Software Foundation: https://gitlab.com/mudar-ca/RotationQuickSetting
