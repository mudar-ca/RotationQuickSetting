package ca.mudar.rotationquicksetting.utils

import android.content.res.Configuration
import androidx.fragment.app.FragmentActivity

fun FragmentActivity.isNightMode(): Boolean =
    resources.configuration.uiMode and
            Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
