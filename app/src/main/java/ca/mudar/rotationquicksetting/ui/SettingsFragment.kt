/*
 * Rotation Quick Setting
 * Add a Quick Settings tile to select portrait or landscape screen orientation.
 *
 * Copyright (C) 2017 Mudar Noufal <mn@mudar.ca>
 *
 * This file is part of RotationQS.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.rotationquicksetting.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceGroup
import ca.mudar.rotationquicksetting.BuildConfig
import ca.mudar.rotationquicksetting.Const
import ca.mudar.rotationquicksetting.Const.PrefsNames
import ca.mudar.rotationquicksetting.R
import ca.mudar.rotationquicksetting.data.UserPrefs
import ca.mudar.rotationquicksetting.utils.OrientationUtils.ROTATION_LAND
import ca.mudar.rotationquicksetting.utils.OrientationUtils.ROTATION_LAND_REVERSE
import ca.mudar.rotationquicksetting.utils.OrientationUtils.ROTATION_PORT
import ca.mudar.rotationquicksetting.utils.OrientationUtils.ROTATION_PORT_REVERSE
import ca.mudar.rotationquicksetting.utils.PermissionUtils
import com.google.android.material.snackbar.Snackbar
import java.util.Locale

class SettingsFragment : PreferenceFragmentCompat() {

    private var listener: SettingsAboutCallback? = null

    private val prefsChangeListener =
        SharedPreferences.OnSharedPreferenceChangeListener { sharedPrefs, key ->
            val reversed = sharedPrefs.getBoolean(key, false)

            val orientation = when (key) {
                PrefsNames.REVERSE_PORT -> when {
                    reversed -> ROTATION_PORT_REVERSE
                    else -> ROTATION_PORT
                }
                PrefsNames.REVERSE_LAND -> when {
                    reversed -> ROTATION_LAND_REVERSE
                    else -> ROTATION_LAND
                }
                else -> return@OnSharedPreferenceChangeListener
            }

            showTryRotationSnackbar(orientation)
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        listener = context as? SettingsAboutCallback
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.sharedPreferencesName = Const.APP_PREFS_NAME
        preferenceManager.sharedPreferencesMode = Context.MODE_PRIVATE

        addPreferencesFromResource(R.xml.prefs_settings)

        preferenceManager?.sharedPreferences?.registerOnSharedPreferenceChangeListener(
            prefsChangeListener
        )

        hideHelpIfPossible(requireContext())
    }

    override fun onResume() {
        super.onResume()

        setupSummaries()
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean =
        when (preference?.key) {
            PrefsNames.PERMISSION_GRANTED -> {
                startActivity(PermissionUtils.getPermissionIntent(context))
                true
            }
            PrefsNames.HELP -> {
                showHelp()
                true
            }
            PrefsNames.ABOUT -> {
                listener?.let {
                    it.onShowAbout()
                    true
                } ?: false
            }
            else -> false
        }

    override fun onDestroy() {
        super.onDestroy()

        // Remove the listener
        preferenceManager?.sharedPreferences?.unregisterOnSharedPreferenceChangeListener(
            prefsChangeListener
        )
    }

    private fun setupSummaries() {
        findPreference<Preference>(PrefsNames.PERMISSION_GRANTED)?.setSummary(
            getPermissionGrantedSummary()
        )
        findPreference<Preference>(PrefsNames.ABOUT)?.summary =
            getString(R.string.prefs_about_summary, BuildConfig.VERSION_NAME)
    }

    @StringRes
    private fun getPermissionGrantedSummary(): Int {
        return when (Settings.System.canWrite(context)) {
            true -> R.string.prefs_permission_granted_on
            false -> R.string.prefs_permission_granted_off
        }
    }

    private fun setOrientation(orientation: Int) {
        if (Settings.System.canWrite(context).not()) {
            return
        }

        context?.contentResolver?.let { contentResolver ->
            Settings.System.putInt(contentResolver, Settings.System.ACCELEROMETER_ROTATION, 0)
            Settings.System.putInt(contentResolver, Settings.System.USER_ROTATION, orientation)
        }
    }

    private fun showTryRotationSnackbar(orientation: Int) {
        if (Settings.System.canWrite(context).not()) {
            return
        }

        val message = when (orientation) {
            ROTATION_PORT -> R.string.snackbar_try_rotation_port
            ROTATION_PORT_REVERSE -> R.string.snackbar_try_rotation_port_reverse
            ROTATION_LAND -> R.string.snackbar_try_rotation_land
            ROTATION_LAND_REVERSE -> R.string.snackbar_try_rotation_land_reverse
            else -> return
        }

        this.view?.let { view ->
            Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.btn_try_it) { setOrientation(orientation) }
                .setActionTextColor(
                    ContextCompat.getColor(view.context, R.color.snackbar_action_text)
                )
                .show()
        }
    }

    private fun showHelp() {
        val language = Locale.getDefault().language
        val viewIntent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(resources.getString(R.string.url_help_quick_settings, language))
        }
        startActivity(viewIntent)
    }

    private fun hideHelpIfPossible(context: Context) {
        val prefs = UserPrefs(context)
        if (prefs.hasHelp().not()) {
            findPreference<PreferenceGroup>(PrefsNames.CAT_GENERAL)
                ?.removePreference(findPreference(PrefsNames.HELP))
            prefs.setHelpCompleted()
        }
    }

    interface SettingsAboutCallback {
        fun onShowAbout()
    }

    companion object {
        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }
}
